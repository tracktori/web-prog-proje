﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MuhendislikFirmasi.Controllers
{
    public class HomeController : Controller
    {
        MuhendislikFirmasiEntities db = new MuhendislikFirmasiEntities();
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult _SliderView()
        {
          var liste = db.Sliders.OrderByDescending(x => x.SliderID);
            return View(liste);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}